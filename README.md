# Dazn-movies

## Run Locally
To run locally, clone the repository on the first place and then execute:

```sh
yarn

yarn start
```

## Scripts

##### `npm run dev` or `yarn dev`
Spins up a local server for local development.
##### `npm run build` or `yarn build`
Builds the app for production to the build folder.
##### `npm run lint` or `yarn lint`
Performs a linting check with recommended settings.
##### `npm run flow` or `yarn flow`
Spins up a flow server and checks for type errors.
##### `npm run test` or `yarn test`
Runs the unit tests.
##### `npm run deploy` or `yarn deploy`
Deploys a static web site into [now](https://zeit.co/now).

## Deployed version
Deployed version can be found on https://dazn-movies-hvxqntojtu.now.sh/

## External component libraries
- [react-autosuggest](https://github.com/moroshko/react-autosuggest) for WAI-ARIA compliant autosuggest component
- [react-icons](https://github.com/react-icons/react-icons)

## Notes
- The search includes autocomplete functionality
- There is basic pagination implemented with infinite scrolling.
- Unit tests have been created for the` SearchAutoSuggest` component. The container component `App` hasn't been tested due to lack of time.
- CSS Modules used for styling.
- Flow types instead of propTypes.
- For simplicity, server and client haven't been split up into separate directories.
- The effective time spent on the exercise has been ~4.5 hours.