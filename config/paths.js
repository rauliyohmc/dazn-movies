const path = require('path');
const fs = require('fs');

/* Computes the canonical pathname by resolving ., .. and symbolic links. Process.cwd(),
refers to the directory from which we invoked the node command (where package.json is) */

const appDirectory = fs.realpathSync(process.cwd());

const resolveApp = relativePath =>
  path.resolve(appDirectory, relativePath);

module.exports = {
  appSrc: resolveApp('src'),
  appBuild: resolveApp('build'),
  appPublic: resolveApp('public'),
  appHtml: resolveApp('public/index.html'),
  appIndexJs: resolveApp('src/index.js'),
  appNodeModules: resolveApp('node_modules'),
};