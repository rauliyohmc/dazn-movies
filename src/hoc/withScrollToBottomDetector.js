// @flow
import * as React from 'react';

type State = {
  hasReachedBottom: boolean,
};

/**
 * HOC that detects when we have reached the bottom of the page
 * The threshold can be configured by using @param bottomOffset
 */
const withScrollToBottomDetector = ({
  bottomOffset = 200,
}: { bottomOffset: number } = {}) => (
  ComposedComponent: React.ComponentType<any>,
): React.ComponentType<any> =>
  class ScrollDecorator extends React.Component<void, State> {
    state = {
      hasReachedBottom: false,
    };

    componentDidMount() {
      window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll = () => {
      // $FlowFixMe
      const { offsetHeight: totalHeight } = document.documentElement;
      // $FlowFixMe
      const offset = document.body.scrollTop + window.innerHeight;
      if (offset + bottomOffset >= totalHeight) {
        this.setState({
          hasReachedBottom: true,
        });
      } else {
        this.setState({
          hasReachedBottom: false,
        });
      }
    };

    render() {
      const { hasReachedBottom } = this.state;
      return (
        <ComposedComponent
          {...this.props}
          hasReachedBottom={hasReachedBottom}
        />
      );
    }
  };

export default withScrollToBottomDetector;
