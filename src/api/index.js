// @flow
const API_ROOT = 'https://api.themoviedb.org/3';
const API_KEY: string = process.env.API_KEY || '';

type Response<T> = {
  results: Array<T>,
  page: number,
};

export type ResponseListItem = {
  id: number,
  title: string,
  overview: string,
  release_date: string,
  vote_average: number,
  poster_path: string,
};

// For simplicity, we are not handling any server errors.
async function callApi(endPoint: string, options: Object = {}) {
  const response = await fetch(`${API_ROOT}/${endPoint}`, options);
  const responseJson = await response.json();
  if (!response.ok) {
    // code != 2xx
    throw responseJson;
  }
  return responseJson;
}

export async function getMovies(
  query: string,
  page: number = 1,
): Promise<Response<ResponseListItem>> {
  return callApi(`search/movie?api_key=${API_KEY}&query=${query}&page=${page}`);
}
