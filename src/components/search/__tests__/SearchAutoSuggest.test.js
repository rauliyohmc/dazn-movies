import React from 'react';
import { mount } from 'enzyme';
import SearchAutoSuggest, { INPUT_DEBOUNCE_TIME } from '../SearchAutoSuggest';

const delay = time =>
  new Promise(resolve => {
    setTimeout(resolve, time);
  });

describe('SearchAutoSuggest component', () => {
  let props;

  beforeEach(() => {
    props = {
      suggestions: [],
      onInputChange: jest.fn(),
      onInputEnterPress: jest.fn(),
      onSuggestionsClearRequested: jest.fn(),
      renderSuggestion: () => <div>Content</div>,
      getSuggestionValue: () => 'Dummy',
    };
  });

  it('Should call onInputChange prop when input value changes, after the debounce time', async () => {
    const wrapper = mount(<SearchAutoSuggest {...props} />);
    wrapper
      .find('input')
      .simulate('change', { target: { value: 'My new value' } });

    await delay(INPUT_DEBOUNCE_TIME);
    expect(wrapper.props().onInputChange).toHaveBeenCalledWith('My new value');
  });

  it('should call onInputEnterPress prop when user presses the enter key', () => {
    const wrapper = mount(<SearchAutoSuggest {...props} />);
    wrapper
      .find('input')
      .simulate('change', { target: { value: 'Interstellar' } });
    wrapper.find('input').simulate('keyPress', { key: 'Enter' });

    expect(wrapper.props().onInputEnterPress).toHaveBeenCalledWith(
      'Interstellar',
    );
  });
});
