// @flow
import React from 'react';
import Autosuggest from 'react-autosuggest';
import debounce from 'lodash.debounce';
import theme from './theme.css';

export const INPUT_DEBOUNCE_TIME = 300;

type Props = {
  suggestions: Array<*>,
  onInputChange: string => mixed,
  onInputEnterPress: string => mixed,
  onSuggestionsClearRequested: () => mixed,
  renderSuggestion: (*) => React$Element<*>,
  getSuggestionValue: Object => string,
};

type State = {
  value: string,
};

class SearchAutoSuggest extends React.Component<Props, State> {
  state = {
    value: '',
  };

  onChange = (event: *, { newValue }: *) => {
    this.setState({
      value: newValue,
    });
  };

  onKeyPress = (event: *) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      this.props.onInputEnterPress(this.state.value);
    }
  };

  onSuggestionsFetchRequested = debounce(
    ({ value }) => this.props.onInputChange(value),
    INPUT_DEBOUNCE_TIME,
    {
      leading: false,
      trailing: true,
    },
  );

  onSuggestionsClearRequested = () => {
    this.props.onSuggestionsClearRequested();
  };

  render() {
    const { value } = this.state;
    const { suggestions } = this.props;

    const inputProps = {
      placeholder: 'Type a movie name',
      value,
      onChange: this.onChange,
      onKeyPress: this.onKeyPress,
    };

    return (
      <Autosuggest
        theme={theme}
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={this.props.getSuggestionValue}
        renderSuggestion={this.props.renderSuggestion}
        inputProps={inputProps}
      />
    );
  }
}

export default SearchAutoSuggest;
