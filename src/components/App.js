// @flow
import React from 'react';
import Layout from './layout/Layout';
import MovieList from './list/MovieList';
import SearchAutoSuggest from './search/SearchAutoSuggest';
import { getMovies, type ResponseListItem } from '../api';

type State = {
  suggestions: Array<ResponseListItem>,
  results: Array<ResponseListItem>,
};

class App extends React.Component<void, State> {
  state = {
    suggestions: [],
    results: [],
  };

  page: number = 1;
  hasMoreData: boolean = true;
  lastQuery: string = '';

  handleBottomReached = () => {
    if (this.hasMoreData) {
      this.fetchMoreData();
    }
  };

  async fetchMoreData() {
    this.page = this.page + 1;
    const data = await getMovies(this.lastQuery, this.page);
    this.hasMoreData = data.results.length > 0;
    this.setState(state => ({
      results: [...state.results, ...data.results],
    }));
  }

  getSuggestionValue = (suggestion: *) => suggestion.title;

  handleInputChange = async (value: string) => {
    const data = await getMovies(value);
    const suggestions = data.results.slice(0, 10);
    this.setState({
      suggestions,
    });
  };

  handleInputEnterPress = async (value: string) => {
    this.lastQuery = value;
    this.page = 1;
    const { results } = await getMovies(value);
    this.setState({
      results,
    });
  };

  handleSuggestionsClear = () => {
    this.setState({
      suggestions: [],
    });
  };

  renderSuggestion = (suggestion: ResponseListItem) => (
    <div>{suggestion.title}</div>
  );

  render() {
    const { suggestions, results } = this.state;
    return (
      <Layout>
        <SearchAutoSuggest
          suggestions={suggestions}
          getSuggestionValue={this.getSuggestionValue}
          onInputChange={this.handleInputChange}
          onInputEnterPress={this.handleInputEnterPress}
          onSuggestionsClearRequested={this.handleSuggestionsClear}
          renderSuggestion={this.renderSuggestion}
        />
        <MovieList data={results} onBottomReached={this.handleBottomReached} />
      </Layout>
    );
  }
}

export default App;
