// @flow
import React from 'react';
import styles from './MovieCard.css';

type Props = {
  name: string,
  description: string,
  imageUrl: string,
  rating: number,
  date: string,
};

const good = '#43A047';
const average = '#FDD835';
const bad = '#FF5722';

function getColor(rating: number) {
  if (rating < 5) {
    return bad;
  } else if (rating < 7) {
    return average;
  }
  return good;
}

export default function MovieCard({
  name,
  description,
  imageUrl,
  rating,
  date,
}: Props) {
  const ratingColor = getColor(rating);
  return (
    <div className={styles.container}>
      <img className={styles.image} src={imageUrl} alt="cover" />
      <div className={styles.content}>
        <div className={styles.row}>
          <span className={styles.rating} style={{ borderColor: ratingColor }}>
            {rating}
          </span>
          <div className={styles.column}>
            <p>{name}</p>
            <p>{date}</p>
          </div>
        </div>
        <p>{description}</p>
      </div>
    </div>
  );
}
