// @flow
import React from 'react';
import MovieCard from './MovieCard';
import type { ResponseListItem } from '../../api';
import styles from './MovieList.css';
import withScrollToBottomDetector from '../../hoc/withScrollToBottomDetector';

type Props = {
  data: Array<ResponseListItem>,
  hasReachedBottom: boolean,
  onBottomReached: () => mixed,
};

class MovieList extends React.Component<Props> {
  componentDidUpdate(prevProps: Props) {
    if (!prevProps.hasReachedBottom && this.props.hasReachedBottom) {
      this.props.onBottomReached();
    }
  }
  render() {
    const { data } = this.props;
    return (
      <div className={styles.container}>
        {data.map(movie => (
          <MovieCard
            key={movie.id}
            name={movie.title}
            description={movie.overview}
            rating={movie.vote_average}
            date={movie.release_date}
            imageUrl={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
          />
        ))}
      </div>
    );
  }
}

export default withScrollToBottomDetector({ bottomOffset: 300 })(MovieList);
