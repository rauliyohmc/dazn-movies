// @flow
import React from 'react';
import styles from './Footer.css';

export default function Footer() {
  return (
    <footer className={styles.container}>
      <p className={styles.text}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer
        tristique libero ut velit molestie sodales. Suspendisse potenti. Nam
        porta quam metus, a commodo massa posuere nec
      </p>
    </footer>
  );
}
