// @flow
import * as React from 'react';
import Header from './Header';
import Footer from './Footer';
import styles from './Layout.css';

type Props = {
  children: React.Node,
};

export default function Layout({ children }: Props) {
  return (
    <React.Fragment>
      <Header />
      <div className={styles.container}>{children}</div>
      <Footer />
    </React.Fragment>
  );
}
