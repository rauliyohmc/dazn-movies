// @flow
import React from 'react';
import MovieIcon from 'react-icons/lib/md/movie-filter';
import styles from './Header.css';

export default function Header() {
  return (
    <header className={styles.container}>
      <MovieIcon size={30} className={styles.logo} />
      <h2>Dazn Movies</h2>
    </header>
  );
}
